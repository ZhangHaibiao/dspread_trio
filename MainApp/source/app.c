#include "app.h"
#include "ui.h"
#include "trans.h"
#include "utils.h"
#include "emvl2.h"
#include "emv.h"
#include "emvproc.h"
#include "cls.h"
#include "aa.h"
#include "Liteui.h"

#include <errno.h>

#ifdef APP_DEBUG
#define APP_STRLOG DBG_STR
#define APP_HEXLOG DBG_HEX
#else
#define APP_STRLOG  
#define APP_HEXLOG 
#endif

#define  EMVL2_ADDR     		   0x14138220

u8 g_load_param_flag = 0;

static ST_TRANLOG m_translog;
extern int test_printer(void * pxPARAM);
extern int Wlan_TestMenu(void * pxPARAM);
extern int WifiFunTest(void * pxPARAM);
extern int EmvL2FileRead(u32 index, u32 offset, u8 *data, u32 len);
extern int EmvL2FileWrite(u32 index, u32 offset, u8 *data, u32 len);
extern int EmvL2FileDel(u32 startindex, u32 endindex);
extern int EmvL2GetSysTime(SYS_TIME * time);
extern int EmvL2SetSysTime(SYS_TIME * time);
extern int EmvL2GetSysTimer(ST_Timer_UNIT *timer,u32 timeout);
extern int EmvL2SysTimerLeft(ST_Timer_UNIT *timer);
extern int EmvL2GetSysTick();
extern void EmvL2DelayMs(u32 ms);
extern void EmvL2Des(unsigned char *input,unsigned char *output,unsigned char *key, u32 mode);
extern int EmvL2Aes(unsigned char *Input, unsigned char *Output, unsigned char *Aeskey, u32 keyLen, u32 Mode);
extern int EmvL2Rsa(u32 bitsize, const u8 *input, u8 *output, u8 *mod, u32 expLen,u8 *exp);
extern int EmvL2Sha(u32 type,u8 *hash, u32 dataLen, u8 *data);
extern int EmvL2ScExchange(const char *DeviceId, const C_APDU *c_apdu, R_APDU *r_apdu);
extern int EmvL2GetRandom(u8 *rand,u32 len);
extern int EmvL2OfflinePinVerify(u32 offline_type, ST_SCPINKEY *RsaKey, u8 *status_word);

extern int Demo_DispImage(void);
int s_sysStatusProc()
{
    CLcdDispStatus(0);
	Ui_Clear();
	Ui_DispTextLineAlign(0, DISPLAY_CENTER, "close sys status", 1);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, "ok", 0);
	Ui_DispTextLineAlign(2, DISPLAY_RIGHT, "ok", 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, "ok", 0);
	Ui_WaitKey(30000);
	
    CLcdDispStatus(1);
	Ui_Clear();
	Ui_DispTextLineAlign(0, DISPLAY_CENTER, "open sys status", 1);
	Ui_DispTextLineAlign(1, DISPLAY_LEFT, "ok", 0);
	Ui_DispTextLineAlign(2, DISPLAY_RIGHT, "ok", 0);
	Ui_DispTextLineAlign(3, DISPLAY_CENTER, "ok", 0);
	Ui_WaitKey(30000);
	return 0;
}

ST_TRANLOG * App_GetTranLog()
{
	return &m_translog;	
}

void App_InitTran()
{
	byte     bcdtime[10];
	byte     asctime[20];
	ST_TRANLOG * pxLOG = App_GetTranLog();
	
	memset((char *)pxLOG, 0, sizeof(ST_TRANLOG));
	pxLOG->type = 0;
	memset(bcdtime,0,sizeof(bcdtime));	
	GetTime(bcdtime);
	APP_HEXLOG("GetTime:",bcdtime,7);
	memset(asctime,0,sizeof(asctime));	
	Utils_Bcd2Asc(bcdtime,6,asctime);
	memcpy(pxLOG->date,"20",2);
	memcpy(pxLOG->date+2,asctime,6);
	memcpy(pxLOG->time,asctime+6,6);
	strcpy(pxLOG->cardUnit, "CUP");
}

int App_CardMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30];
	ST_MENUITEM menu[4];
	
	if(!g_load_param_flag)
	{
		tmp1[1] = 1;
		Trans_MkskKeyInit(tmp1);
		EMV_InitDefault(tmp1);
		g_load_param_flag = 1;
	}	

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,"mag card test");
	menu[i].text = tmp1;
	menu[i].func= Trans_MagcardTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1';		
	sprintf(tmp2,"%d.%s",i+1,"chip card test");
	menu[i].text = tmp2;
	menu[i].func= Trans_IcccardTest;
	menu[i].param= (void *)NULL;
	i++;

	Ui_Menu("card test", menu, i, APP_UI_TIMEOUT);
  	return APP_RET_NO_DISP;
}

int App_SysMenu(void * pxPARAM)
{	
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30];
	ST_MENUITEM menu[7];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,"MKSk INIT");
	menu[i].text = tmp1;
	menu[i].func= Trans_MkskKeyInit;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1'; 	
	sprintf(tmp2,"%d.%s",i+1,"load emv prm");
	menu[i].text = tmp2;
	menu[i].func= EMV_InitDefault;
	menu[i].param= (void *)NULL;
	i++;	

	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1'; 	
	sprintf(tmp3,"%d.%s",i+1,"sys status test");
	menu[i].text = tmp3;
	menu[i].func= s_sysStatusProc;
	menu[i].param= (void *)NULL;
	i++;	

	Ui_Menu("param set", menu, i, APP_UI_TIMEOUT);	
  	return APP_RET_NO_DISP;
}

int App_lcdflash(void * pxPARAM){

	Ui_ClearBlockColor(0,0,320,240,DISPLAY_COLOR_RED);
	DelayMs(500);
	Ui_ClearBlockColor(0,0,320,240,DISPLAY_COLOR_GRAY);
	DelayMs(500);
	Ui_ClearBlockColor(0,0,320,240,DISPLAY_COLOR_BLACK);
	DelayMs(500);
	Ui_ClearBlockColor(0,0,320,240,DISPALY_COLOR_GREEN);
	DelayMs(500);
	Ui_ClearBlockColor(0,0,320,240,DISPLAY_COLOR_WHITE);
	DelayMs(500);
	Ui_DispBMP(88, 108, 144, 72, cls_array);
	DelayMs(500);
	Ui_DispBMP(0, 0, 320, 240, aa_array);
	DelayMs(500);
	return 0;
}

int AboutVersion(void * pxPARAM)
{
	u8 ver[10]={0},strver[48]={0};
	u8 info[30] = {0};

	memset(ver,0,sizeof(ver));
	ReadVerInfo(ver);
	Ui_Clear();
	Ui_DispTitle("Terminal Information");
	//Ui_DispTextLineAlign(1, DISPLAY_CENTER, "Terminal Information", 0);
	memset(strver,0,sizeof(strver));
	sprintf(strver,"APP:%s\0",APP_VER);
	Ui_DispTextLineAlign(3, DISPLAY_LEFT, strver, 0);
	memset(strver,0,sizeof(strver));
	sprintf(strver,"sbi:%d.%d OS:%d.%d.%d%%",ver[0],ver[1],ver[2],ver[3],ver[4]);
	Ui_DispTextLineAlign(5, DISPLAY_LEFT, strver, 0);

	GetTermInfo(info);
	u8* disp_str = (info[21] == 0x00?"Debug Mode Device":"NoDebug Mode Device");
	Ui_DispTextLineAlign(7, DISPLAY_LEFT, disp_str, 0);

	Ui_WaitKey(5000);	

	return 0;
}

int Display_Image(void * pxPARAM)
{
	Demo_DispImage();
	return 0;

}


int keyBoardTest(void * pxPARAM)
{
	char keyValue;
	char string[128];

	Ui_Clear();
	Ui_DispText(0,2*24,"wait for key", 0);
	while (1)
	{
		keyValue = getkey();
		if(keyValue == KEYENTER)
		{
			break;
		}
		
		memset(string, 0, sizeof(string));
		sprintf(string, "Key:0x%02X", keyValue);

		Ui_Clear();
		Ui_DispText(0,2*24,string, 0);
		Ui_DispText(0,4*24,"key\"Enter\"(0x0d) to exit", 0);
	}
	
}

extern int FontAlignTest(void * pxPARAM);
extern int FontDisplayWithXY(void * pxPARAM);
extern int FontSetAttr(void * pxPARAM);
extern int LcdBackGroundTest(void * pxPARAM);
extern int printerTest(void * pxPARAM);
extern int icon_unLockTest(void * pxPARAM);
extern int backLitghtTest(void * pxPARAM);
extern int 	ota_demo(void * pxPARAM);


int Other_Func(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30],tmp5[30],tmp6[30],tmp7[30],tmp8[30],tmp9[30];
	ST_MENUITEM menu[9];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,"A-2/3 Align Direction");
	menu[i].text = tmp1;
	menu[i].func= FontAlignTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1';
	sprintf(tmp2,"%d.%s",i+1,"A-4/5/6 XY Retract PNG");
	menu[i].text = tmp2;
	menu[i].func= FontDisplayWithXY;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,"A-8/16 SetFontAttr");
	menu[i].text = tmp3;
	menu[i].func= FontSetAttr;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,"A-27/28 background clear");
	menu[i].text = tmp4;
	menu[i].func= LcdBackGroundTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp5,0,sizeof(tmp5));
	menu[i].kb = i+'1';
	sprintf(tmp5,"%d.%s",i+1,"printf");
	menu[i].text = tmp5;
	menu[i].func= printerTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp6,0,sizeof(tmp6));
	menu[i].kb = i+'1';
	sprintf(tmp6,"%d.%s",i+1,"icon bar unlock");
	menu[i].text = tmp6;
	menu[i].func= icon_unLockTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp7,0,sizeof(tmp7));
	menu[i].kb = i+'1';
	sprintf(tmp7,"%d.%s",i+1,"LCD BackLitght");
	menu[i].text = tmp7;
	menu[i].func= backLitghtTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp8,0,sizeof(tmp8));
	menu[i].kb = i+'1';
	sprintf(tmp8,"%d.%s",i+1,"OTA Demo");
	menu[i].text = tmp8;
	menu[i].func= ota_demo;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp9,0,sizeof(tmp9));
	menu[i].kb = i+'1';
	sprintf(tmp9,"%d.%s",i+1,"KeyBoard");
	menu[i].text = tmp9;
	menu[i].func= keyBoardTest;
	menu[i].param= (void *)NULL;
	i++;



	return Ui_Menu("Other Func", menu, i, APP_UI_TIMEOUT);

}


int App_TransMenu(void * pxPARAM)
{
	int i=0;
	char tmp1[30],tmp2[30],tmp3[30],tmp4[30],tmp5[30],tmp6[30],tmp7[30],tmp8[30],tmp9[30];
	ST_MENUITEM menu[18];

	memset(tmp1,0,sizeof(tmp1));
	menu[i].kb = i+'1';
	sprintf(tmp1,"%d.%s",i+1,"card test");
	menu[i].text = tmp1;
	menu[i].func= App_CardMenu;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp2,0,sizeof(tmp2));
	menu[i].kb = i+'1';
	sprintf(tmp2,"%d.%s",i+1,"param set");
	menu[i].text = tmp2;
	menu[i].func= App_SysMenu;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp3,0,sizeof(tmp3));
	menu[i].kb = i+'1';
	sprintf(tmp3,"%d.%s",i+1,"lcd flash");
	menu[i].text = tmp3;
	menu[i].func= App_lcdflash;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp4,0,sizeof(tmp4));
	menu[i].kb = i+'1';
	sprintf(tmp4,"%d.%s",i+1,"printer test");
	menu[i].text = tmp4;
	menu[i].func= test_printer;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp5,0,sizeof(tmp5));
	menu[i].kb = i+'1';
	sprintf(tmp5,"%d.%s",i+1,"wlan test");
	menu[i].text = tmp5;
	menu[i].func= Wlan_TestMenu;
	menu[i].param= (void *)NULL;
	i++;
	
	memset(tmp6,0,sizeof(tmp6));
	menu[i].kb = i+'1';
	sprintf(tmp6,"%d.%s",i+1,"wifi test");
	menu[i].text = tmp6;
	menu[i].func= WifiFunTest;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp8,0,sizeof(tmp8));
	menu[i].kb = i+'1';
	sprintf(tmp8,"%d.%s",i+1,"Other Func");
	menu[i].text = tmp8;
	menu[i].func= Other_Func;
	menu[i].param= (void *)NULL;
	i++;

	memset(tmp7,0,sizeof(tmp7));
	menu[i].kb = i+'1';
	sprintf(tmp7,"%d.%s",i+1,"Version");
	menu[i].text = tmp7;
	menu[i].func= AboutVersion;
	menu[i].param= (void *)NULL;
	i++;

	#if 0
	memset(tmp9,0,sizeof(tmp9));
	menu[i].kb = i+'1';
	sprintf(tmp9,"%d.%s",i+1,"MyTest");
	menu[i].text = tmp9;
	menu[i].func= MyTestFunc;
	menu[i].param= (void *)NULL;
	i++;
	#endif

	return Ui_Menu("main menu", menu, i, APP_UI_TIMEOUT);
}

void s_IconUpdateProc(u32 ex_period_ms)
{
    u32 flash_period=3000;//ms
    static u32 cnt=0;
	u8 sign_level;

    if(ex_period_ms) {
        if(cnt++%(flash_period/ex_period_ms)) return; 
    }
	WlGetSignal(&sign_level);
	BatteryCheck();
}

const u32 api_array[] = {
    //platform api
    (u32)usb_debug,
    (u32)EmvL2FileRead,
    (u32)EmvL2FileWrite,
	(u32)EmvL2FileDel,
	(u32)EmvL2GetSysTime,
	(u32)EmvL2SetSysTime,
	(u32)EmvL2GetSysTimer,
	(u32)EmvL2SysTimerLeft,
	(u32)EmvL2GetSysTick,
	(u32)EmvL2DelayMs,
	(u32)EmvL2Des,
	(u32)EmvL2Aes,
	(u32)EmvL2Rsa,
	(u32)EmvL2Sha,
	(u32)EmvL2ScExchange,
	(u32)EmvL2GetRandom,
	(u32)EmvL2OfflinePinVerify,
};

int run_emv_kernel()
{
	u32 emventry;
	u32 (*fun_entry)(void *funs);
	
	fun_entry = (void*)EMVL2_ADDR;
	emventry = fun_entry((void*)api_array);
	init_emv_api(emventry);

    return 0;
}

void App_Main(void)
{
    u32 regval, cnt;
    u8 kb, buf[32];
    ST_LCD_INFO LcdInfo;
    int ret;
    u8 rsp[64];
    int type;

	run_emv_kernel();
    LiteMapiInit();
	
	Ui_Clear();
	Ui_ClearKey();
	Ui_DispTextLineAlign(2, DISPLAY_LEFT, "param init...", 0);
	EMV_InitCore();

    while(1) {
		s_IconUpdateProc(0);
		App_TransMenu((void *)NULL);
		DBG_STR("while begin");
    }
    APP_STRLOG("APP Exist");
    return;
}
