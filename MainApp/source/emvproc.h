#ifndef __EMVPROC_H__
#define __EMVPROC_H__

void EMV_InitCAPK();
void EMV_InitAidList();
int EMV_Process(int cardType);
int EMV_Process_Simple(int cardType);
int EMV_QPSProcess();

int EMV_SelectApp(int isRetry, int num, ST_CANDLIST * appList);
int EMV_CheckId(byte type, byte * no);
int EMV_InputOnlinePIN();
int EMV_ReadCardData();
int EMV_InitDefault(void * pxPARAM);

#endif

